<?php 
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
    <?php print $head ?>
    <title><?php print $head_title ?></title>
    <?php print $styles ?>
    <?php print $scripts ?>
  </head>

<body id="home">
<div id="page" class="clearfloat">

  <div class="clearfloat"><div id="branding" class="left">
    <?php if ($logo) { print '<img src="'. check_url($logo) .'" alt="'. $site_title .'" id="logo" />'; }?>
    <div class="blogtitle"><a href="/"><?php print $site_name ?></a></div>
    <div class="description"><?php print check_plain($site_name) ?> </div>
  </div>
  <div class="right">
    <?php if ($search_box): ?><?php print $search_box ?><?php endif; ?>
  </div></div>

<?php if (isset($primary_links)) : ?>
 <?php print theme('links', $primary_links, array('class' => 'primary-links clearfloat') , $logged_in, TRUE, TRUE, $user) ?>
<?php endif; ?>

<?php if (isset($secondary_links)) : ?>
  <?php print theme('links', $secondary_links, array('class' => 'secondary-links clearfloat')) ?>
<?php endif; ?>

<?php if ($tabs): ?>
  <?php print '<div id="tabs" class="clear-block">' . $tabs .'</div>' ?>
<?php endif; ?>

<?php print $breadcrumb; ?>

<?php if (($topleft) || ($topright)) : ?>
  <div id="homecontent-top">
    <div id="homecontent-topleft" class="left">
      <?php print $topleft?>
    </div>
    <div id="homecontent-topright" class="right">
      <?php print $topright ?>
    </div>
  </div>
  <div class="clear"></div>
  <hr/>
<?php endif; ?>

<div class="clear"></div>

<?php if ($topad) : ?>
  <?php if (!($topleft) and !($topright)) : ?>
    <hr/>
  <?php endif; ?>
  <div class="topad">
    <?php print $topad ?>
  </div>
  <hr/>
<?php endif; ?>

<?php if (($layout == 'both') || ($layout == 'right') || ($layout == 'left')): ?>
  <div id="section-content">
    <?php if ($title): print '<h2>'. $title .'</h2>'; endif; ?>
    <?php print $help; ?>
    <?php if ($show_messages && $messages): print $messages; endif; ?>
    <?php print $contentop ?>
    <?php print $content ?>
    <?php print $contebot ?>
  </div>
  <div id="sidebar">
    <?php print $right ?>
  </div>
<?php else : ?>
  <div id="pagecontent">
    <?php if ($title): print '<h2>'. $title .'</h2>'; endif; ?>
    <?php print $help; ?>
    <?php if ($show_messages && $messages): print $messages; endif; ?>
    <?php print $contentop ?>
    <?php print $content ?>
    <?php print $contebot ?>
  </div>
<?php endif; ?>

<div class="clear"></div>

<?php if ($botad) : ?>
  <hr/>
  <div class="topad">
    <?php print $botad ?>
  </div>
  <?php if (!($botleft) or !($botcenter) or !($botright)) : ?>
    <hr/>
  <?php endif; ?>
<?php endif; ?>

<?php // 3 columns
if (($botleft) or ($botcenter) or ($botright)) : ?>
 <hr/>
 <div id="pageleft">  <?php print $botleft ?> </div>
 <div id="pagemiddle"> <?php print $botcenter ?> </div>
 <div id="pageright"> <?php print $botright ?> </div>
<?php endif; ?>

</div>
             
<?php if ($footer) : ?>
<div id="footer">
  <div><?php print $footer_message . $footer ?></div>
  <?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>
</div>
<?php endif; ?>

<?php print $closure ?>

  </body>
</html>