<?php 
 ?>
<div class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">

<?php print $picture ?>

<?php if ($page == 0): ?>
  <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
<?php endif; ?>

<?php if ($submitted): ?>
  <div class="submitted"><?php print $name . ' (' .  format_date($created, "small") . ')'; ?></div>
<?php endif; ?>

<div class="content clear-block">
  <?php print $content ?>
</div>

<?php if ($taxonomy): ?>
  <div class="terms"><?php print $terms ?></div>
<?php endif;?>

<?php if ($node->comment_count): ?>
<h2><? print t('Comments') . ' (' . $node->comment_count . ')';?></h2>
<?php endif;?>


<?php if ($links): ?>
  <div class="links"><?php print $links; ?></div>
<?php endif; ?>

</div>
