<?php 
 ?>
<?php
/**
 * Return a themed set of links.
 *
 * @param $links
 *   A keyed array of links to be themed.
 * @param $attributes
 *   A keyed array of attributes
 * @return
 *   A string containing an unordered list of links.
 */
function phptemplate_links($links, $attributes = array('class' => 'links'), $logged_in = '', $login = FALSE,  $account = TRUE, $user = '') {
  global $language;
  $output = '';

  if (count($links) > 0) {
    $output = '<ul'. drupal_attributes($attributes) .'>';

    $num_links = count($links);
    $i = 1;

    foreach ($links as $key => $link) {
      $class = '';/*$key; */

      // Add first, last and active classes to the list of links to help out themers.
      if ($i == 1) { $class .= ' first'; }
      if ($i == $num_links) { $class .= ' last'; }
      if (isset($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && drupal_is_front_page()))) { $class .= ' active'; }

      if ($link['attributes']['class'] == 'active-trail') { $class .= ' active'; }

      if ($class == '') {
        $output .= '<li>';
      } else {
        $output .= '<li'. drupal_attributes(array('class' => $class)) .'>';
      }
      if (isset($link['href'])) {
        // Pass in $link as $options, they share the same keys.
        $output .= l($link['title'], $link['href'], $link);
      }
      else if (!empty($link['title'])) {
        // Some links are actually not links, but we wrap these in <span> for adding title and class attributes
        if (empty($link['html'])) {
          $link['title'] = check_plain($link['title']);
        }
        $span_attributes = '';
        if (isset($link['attributes'])) {
          $span_attributes = drupal_attributes($link['attributes']);
        }
        $output .= '<span'. $span_attributes .'>'. $link['title'] .'</span>';
      }

      $i++;
      $output .= "</li>\n";
    }
  
    if ($login) {
      if ($logged_in) {
        $output .= '<li id="login"><a href="/logout">' .  t('Log Out') . '</a></li>';
      }
      else {
        $output .= '<li id="login"><a href="/user">' .  t('Login') . '</a></li>';
      }
    }

    if ($logged_in AND $user) {
      $output .= '<li id="user">' . l( "($user->name)", "user/$user->uid") . '</li>';         
    }

    if ($logged_in AND $account) {
      $output .= '<li id="new_content"><a href="/node/add">' . t('Create content')  . '</a></li>';
    }

    $output .= '</ul>';
  }

  return $output;
}


/**
 * Insert Google Ad Manager scripts into the page
 */
function phptemplate_preprocess_page(&$vars) {
  if (module_exists('google_admanager')) {
    $vars['scripts'] .= google_admanager_add_js();
  }

}


/**
 * Return a themed Drupal pager.
 */
function  wynton_magazine_pager($tags = array(), $limit = 10, $element = 0, $parameters = array(), $quantity = 9) {
  global $pager_page_array, $pager_total;
  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // first is the first page listed by this pager piece (re quantity)
  $pager_first = $pager_current - $pager_middle + 1;
  // last is the last page listed by this pager piece (re quantity)
  $pager_last = $pager_current + $quantity - $pager_middle;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.
 
  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }
  // End of generation loop preparation.
  $li_previous = theme('pager_previous', (isset($tags[1]) ? $tags[1] : t('<<')), $limit, $element, 1, $parameters);
  $li_next = theme('pager_next', (isset($tags[3]) ? $tags[3] : t('>>')), $limit, $element, 1, $parameters);
 
  $li_first = theme('pager_first', 1, $limit, $element, $parameters);
  $li_last = theme('pager_last', $pager_max, $limit, $element, $parameters);
 
  $show_first = ( $pager_current > $pager_middle ) ? true : false ;
  $show_last = (  $pager_current < ( $pager_max - $pager_middle + 1 ) ) ? true : false ;
 
  if ($pager_total[$element] > 1) {
 
    if ( $li_previous ) $items[] = array( 'class' => 'pager-previous', 'data' => $li_previous );
    if ( $show_first && $li_first ) $items[] = array( 'class' => 'pager-first', 'data' => $li_first );
 
    // When there is more than one page, create the pager list.
    if ($i != $pager_max) {
 
      if ($i > 2) $items[] = array( 'class' => 'pager-ellipsis', 'data' => '<span>...</span>' );
 
      // Now generate the actual pager piece.
      for (; $i <= $pager_last && $i <= $pager_max; $i++) {
        if ($i < $pager_current ) $items[] = array( 'class' => 'pager-item', 'data' => theme('pager_previous', $i, $limit, $element, ($pager_current - $i), $parameters) );
        if ($i == $pager_current) $items[] = array( 'class' => 'pager-current', 'data' => '<span>' . $i . '</span>' );
        if ($i > $pager_current) $items[] = array( 'class' => 'pager-item', 'data' => theme('pager_next', $i, $limit, $element, ($i - $pager_current), $parameters) );
      }
 
      if ($i < $pager_max) $items[] = array( 'class' => 'pager-ellipsis', 'data' => '<span>...</span>' );
 
    }
 
    // End generation.
    if ( $show_last && $li_last) $items[] = array( 'class' => 'pager-last', 'data' => $li_last );
    if ( $li_next) $items[] = array( 'class' => 'pager-next', 'data' => $li_next );
 
    // Ctrl- back/forward js navigation
    $js = '<script type="text/javascript">'
        . 'var ctrlPressed = false;'
	      . '$(document).keyup( function(event) {'
        . '  if (event.keyCode == 17 && ctrlPressed) ctrlPressed = false;'
        . '});'
        . '$(document).keydown( function(event) {'
        . '  if (event.keyCode == 17 && !ctrlPressed) { ctrlPressed = true; }'
        . '  if ( ctrlPressed ) {'
        . '    var link = null;'
        . '    var href = null;'
        . '    switch (event.keyCode) {'
        . '      case 37: link = $("li.pager-previous a").attr("href"); break;'
        . '      case 39: link = $("li.pager-next a").attr("href"); break;'
        . '    }'
        . '    if (link) document.location = link;'
        . '  }'
        . '});'
        . '</script>';
  
    return theme('item_list', $items, NULL, 'ul', array('class' => 'pager'));
  }
}