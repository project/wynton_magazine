<?php 
 ?>
<div class="comment<?php print ($comment->new) ? ' comment-new' : ''; print ' '. $status; print ' '. $zebra; ?>">

<div class="clear-block">
<?php if ($submitted): ?>
  <div class="submitted"><?php print $author . ' (' .  format_date($comment->timestamp, "small") . ')'; ?></div>
<?php endif; ?>

  <?php if ($comment->new) : ?>
    <span class="new"><?php print drupal_ucfirst($new) ?></span>
  <?php endif; ?>

  <?php print $picture ?>

    <div class="content">
      <?php print $content ?>

      <?php if ($signature): ?>
      <div class="clear-block">
        <div>—</div>
        <?php print $signature ?>
      </div>
      <?php endif; ?>
    </div>
  </div>

<?php if ($links): ?>
  <div class="links"><?php print $links ?></div>
<?php endif; ?>

</div>

